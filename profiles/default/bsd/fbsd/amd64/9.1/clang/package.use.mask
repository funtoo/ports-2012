# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/profiles/default/bsd/fbsd/amd64/9.1/clang/package.use.mask,v 1.3 2013/06/01 14:17:54 aballier Exp $

# Build libcxxrt over libgcc_s since that is what clang defaults to.
sys-libs/libcxxrt libunwind

# Needs to be fixed: cxx useflag enables plugins and gold. 
# gold fails to build with clang:
# https://bugs.gentoo.org/show_bug.cgi?id=427344
# http://llvm.org/bugs/show_bug.cgi?id=12299
sys-devel/binutils cxx
